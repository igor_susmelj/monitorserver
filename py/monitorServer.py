import zmq
import time
from struct import *

#define buffer size for master messages
BUF = 16

#initialize zmq
context = zmq.Context()
monitorServer = context.socket(zmq.PAIR)
masterClient = context.socket(zmq.PAIR)

#connect to master
masterClient.connect("tcp://localhost:5555")

#start monitor server
monitorServer.bind("tcp://*:6060")
print "Started monitor input server"

#start poller
poller = zmq.Poller()
poller.register(masterClient, zmq.POLLIN)
poller.register(monitorServer, zmq.POLLIN)

#send first message to master
masterClient.send("hi master")

serverConnected = False


print "Entering main loop"
while True:
	#check for input messages (wait for max 10 millsec)
	socks = dict(poller.poll(10))

	#if messages for monitorServer handle them
        if monitorServer in socks and socks[monitorServer] == zmq.POLLIN:
		monitorMessage = monitorServer.recv()
                #print "received from client: %s" % monitorMessage
                #forward message to master
                masterClient.send(monitorMessage)
        #if messages for masterClient handle them	
        if masterClient in socks and socks[masterClient] == zmq.POLLIN:
                if serverConnected == False:
                    serverConnected = True
                    print "Now connected with server"
                masterMessage = masterClient.recv()
                #data = unpack('%sf' % (BUF), masterMessage) 
                #print ("received from master: " + masterMessage)
                #print received floating point tuple
                #print tuple(data)
                #forward message to monitor server
                monitorServer.send(masterMessage)
                #masterClient.send("data to monitor sent")
        #wait 10ms to reduce CPU usage
        time.sleep(0.01)
